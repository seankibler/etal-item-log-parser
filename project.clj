(defproject etal-item-log-viewer "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [korma "0.4.0"]
                 [org.xerial/sqlite-jdbc "3.7.15-M1"]
                 [me.raynes/fs "1.4.6"]]
  :main ^:skip-aot etal-item-log-viewer.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
