(ns etal-item-log-viewer.core
  (:gen-class)
  (:require [clojure.xml :as xml]
            [clojure.zip :as zip]
            [clojure.pprint :as pp]
            [clojure.java.io :as io]
            [clojure.string :as st]
            [clojure.set :refer [rename-keys]]
            [korma.db :refer [defdb sqlite3]]
            [korma.core :refer [insert defentity values select]]
            [me.raynes.fs :as fs]
            [etal-item-log-viewer.util :refer [parse-int]]))

(def db (sqlite3 {:db "resources/db/d2.db"}))
(defdb items-db db)
(declare drops)
(defentity drops)

(def quality-names {"1" "White"
                    "2" "Rune"
                    "3" "Superior"
                    "4" "Magic"
                    "5" "Set"
                    "6" "Rare"
                    "7" "Unique"})

(defn zip-str [s]
  (zip/xml-zip
    (xml/parse s)))

(defn add-quality [item]
  (-> item
      :quality
      (#(get quality-names %1))
      (#(assoc item :quality %1))))

(defn add-sockets [item]
  (let [socket-val (-> item
                       :attributes
                       (st/split #"\|")
                       last
                       (st/split #": ")
                       (#(apply array-map %1))
                       (rename-keys {"Sockets" :sockets}))]
    (if (contains? socket-val :sockets)
      (-> socket-val
          :sockets
          parse-int
          (#(assoc item :sockets %1)))
      (assoc item :sockets 0))))

(defn add-picked-up [item]
  (let [picked-val (-> item
                       :attributes
                       (st/split #"\|")
                       first)]
    (assoc item :picked (= picked-val "Item not picked up"))))

(defn extract-keys [item]
  (-> item
      :attrs
      (select-keys [:id :name :ilvl :location :typedesc :quality :time :ethereal :char])
      (rename-keys {:char :toon})
      (rename-keys {:typedesc :description})
      (assoc :attributes (-> item :content first))
      add-quality
      add-picked-up
      add-sockets))

(defn extract-records-keys [items]
  (map extract-keys items))

(defn fix-xml-encoding [path encoding]
 (let [contents (-> (slurp path :encoding encoding)
                    (st/replace-first "iso-8859-1" encoding))]
   (println (subs contents 0 125))
   (spit path contents :encoding encoding)))


(defn persist-items [items]
  (doall (map #(insert drops (values %1)) items)))

(defn process-xml-file [filepath]
  (fix-xml-encoding filepath "utf-16")
  (with-open [data-stream (io/input-stream filepath :encoding "UTF-16")]
    (let [items (-> data-stream zip-str first :content)]
      (-> items
          extract-records-keys
          persist-items))))

(defn -main
  "I eat XML."
  [& args]
  (if-let [files (filter fs/exists? args)]
    (doall
      (pmap process-xml-file files))
    (println "No files given or given files do not exist."))
  (System/exit 0))
