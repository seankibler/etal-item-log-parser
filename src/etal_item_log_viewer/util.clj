(ns etal-item-log-viewer.util)

(defn parse-int [st]
  (try
    (Integer/parseInt st)
    (catch Exception ex nil)))
